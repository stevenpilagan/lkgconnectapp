export const actions = {
    /**
     * 
     * @param {*} state 
     * @param {*} dispatch 
     * @param {*} commit 
     */
    createTask( state, dispatch, commit ) {

    },
    getTask( state, dispatch, commit ) {

    },
    updateTask( state, dispatch, commit ) {

    },
    deleteTask( state, dispatch, commit ) {

    }
}