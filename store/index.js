import Vuex from 'vuex'
import { BriefModule } from './brief'
import { TaskModule } from './task'
import { OpsModule } from './ops'
import { UserModule } from './user'
import { GroupModule } from './group'
import { AclModule } from './acl'

const store = () => {
  return new Vuex.Store({
        modules: {
            brief: BriefModule,
            task: TaskModule,
            ops: OpsModule,
            user: UserModule,
            group: GroupModule,
            acl: AclModule
        }
    })
}

export default store
