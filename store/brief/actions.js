export const actions = {
    /**
     * 
     * @param {*} state 
     * @param {*} dispatch 
     * @param {*} commit 
     */
    createProject( state, dispatch, commit ) {

    },
    getProjects( state, dispatch, commit ) {

    },
    updateProject( state, dispatch, commit ) {

    },
    deleteProject( state, dispatch, commit ) {

    }
}