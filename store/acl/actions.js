export const actions = {
    /**
     * 
     * @param {*} state 
     * @param {*} dispatch 
     * @param {*} commit 
     */
    createAcl( state, dispatch, commit ) {

    },
    getAcls( state, dispatch, commit ) {

    },
    updateAcl( state, dispatch, commit ) {

    },
    deleteAcl( state, dispatch, commit ) {

    }
}