export function projects()  {
    return [{
        "id": 1,
        "ticket": "Zaam-Dox",
        "title": "e-enable collaborative infomediaries",
        "description": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.",
        "phase": {
          "id": 3,
          "title": "PNC Financial Services Group, Inc. (The)"
        },
        "due": "2/16/2019"
      }, {
        "id": 2,
        "ticket": "Lotlux",
        "title": "extend sexy paradigms",
        "description": "In congue. Etiam justo. Etiam pretium iaculis justo.",
        "phase": {
          "id": 2,
          "title": "Dreyfus Strategic Municipal Bond Fund, Inc."
        },
        "due": "1/22/2019"
      }, {
        "id": 3,
        "ticket": "Flowdesk",
        "title": "synthesize seamless relationships",
        "description": "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.",
        "phase": {
          "id": 1,
          "title": "Hersha Hospitality Trust"
        },
        "due": "5/13/2018"
      }, {
        "id": 4,
        "ticket": "Keylex",
        "title": "expedite user-centric architectures",
        "description": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.",
        "phase": {
          "id": 2,
          "title": "TCF Financial Corporation"
        },
        "due": "8/3/2019"
      }, {
        "id": 5,
        "ticket": "Temp",
        "title": "unleash efficient e-commerce",
        "description": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
        "phase": {
          "id": 3,
          "title": "Liquidity Services, Inc."
        },
        "due": "11/30/2019"
      }, {
        "id": 6,
        "ticket": "Subin",
        "title": "scale wireless synergies",
        "description": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.",
        "phase": {
          "id": 3,
          "title": "Hanmi Financial Corporation"
        },
        "due": "4/13/2019"
      }, {
        "id": 7,
        "ticket": "Matsoft",
        "title": "disintermediate out-of-the-box users",
        "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.",
        "phase": {
          "id": 1,
          "title": "Oasis Petroleum Inc."
        },
        "due": "9/5/2017"
      }, {
        "id": 8,
        "ticket": "Lotstring",
        "title": "synergize sticky markets",
        "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "phase": {
          "id": 2,
          "title": "Iridium Communications Inc"
        },
        "due": "5/10/2019"
      }, {
        "id": 9,
        "ticket": "Konklux",
        "title": "streamline impactful platforms",
        "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.",
        "phase": {
          "id": 2,
          "title": "United Rentals, Inc."
        },
        "due": "10/19/2019"
      }, {
        "id": 10,
        "ticket": "Fix San",
        "title": "reintermediate extensible eyeballs",
        "description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.",
        "phase": {
          "id": 2,
          "title": "Fifth Third Bancorp"
        },
        "due": "7/19/2019"
      }, {
        "id": 11,
        "ticket": "Zamit",
        "title": "aggregate efficient web-readiness",
        "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.",
        "phase": {
          "id": 1,
          "title": "WAVE Life Sciences Ltd."
        },
        "due": "11/23/2018"
      }, {
        "id": 12,
        "ticket": "Asoka",
        "title": "evolve efficient systems",
        "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.",
        "phase": {
          "id": 1,
          "title": "BroadSoft, Inc."
        },
        "due": "4/20/2018"
      }, {
        "id": 13,
        "ticket": "Gembucket",
        "title": "whiteboard clicks-and-mortar portals",
        "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
        "phase": {
          "id": 3,
          "title": "Eastman Kodak Company"
        },
        "due": "6/30/2019"
      }, {
        "id": 14,
        "ticket": "Konklux",
        "title": "monetize world-class technologies",
        "description": "Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.",
        "phase": {
          "id": 3,
          "title": "Franklin Resources, Inc."
        },
        "due": "1/28/2019"
      }, {
        "id": 15,
        "ticket": "Trippledex",
        "title": "deliver user-centric solutions",
        "description": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.",
        "phase": {
          "id": 3,
          "title": "Aqua America, Inc."
        },
        "due": "5/6/2018"
      }, {
        "id": 16,
        "ticket": "Pannier",
        "title": "reinvent cutting-edge web services",
        "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "phase": {
          "id": 1,
          "title": "Morgan Stanley"
        },
        "due": "7/12/2017"
      }, {
        "id": 17,
        "ticket": "Ronstring",
        "title": "deploy frictionless partnerships",
        "description": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.",
        "phase": {
          "id": 2,
          "title": "Gores Holdings II, Inc."
        },
        "due": "10/2/2018"
      }, {
        "id": 18,
        "ticket": "Zamit",
        "title": "envisioneer proactive experiences",
        "description": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
        "phase": {
          "id": 3,
          "title": "Simon Property Group, Inc."
        },
        "due": "11/3/2019"
      }, {
        "id": 19,
        "ticket": "Temp",
        "title": "evolve seamless e-services",
        "description": "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.",
        "phase": {
          "id": 1,
          "title": "Pitney Bowes Inc."
        },
        "due": "11/25/2019"
      }, {
        "id": 20,
        "ticket": "Y-Solowarm",
        "title": "leverage plug-and-play eyeballs",
        "description": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
        "phase": {
          "id": 2,
          "title": "Silicom Ltd"
        },
        "due": "4/1/2019"
      }, {
        "id": 21,
        "ticket": "Cardify",
        "title": "evolve global experiences",
        "description": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.",
        "phase": {
          "id": 2,
          "title": "Scudder Strategic Income Trust"
        },
        "due": "1/8/2017"
      }, {
        "id": 22,
        "ticket": "Andalax",
        "title": "redefine ubiquitous schemas",
        "description": "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.",
        "phase": {
          "id": 2,
          "title": "Investors Real Estate Trust"
        },
        "due": "5/30/2019"
      }, {
        "id": 23,
        "ticket": "Tres-Zap",
        "title": "engineer sexy eyeballs",
        "description": "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.",
        "phase": {
          "id": 3,
          "title": "KKR Financial Holdings LLC"
        },
        "due": "5/25/2017"
      }, {
        "id": 24,
        "ticket": "Bigtax",
        "title": "drive end-to-end portals",
        "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\n\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.",
        "phase": {
          "id": 1,
          "title": "Government Properties Income Trust"
        },
        "due": "4/22/2017"
      }, {
        "id": 25,
        "ticket": "Y-find",
        "title": "target integrated synergies",
        "description": "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
        "phase": {
          "id": 2,
          "title": "John Bean Technologies Corporation"
        },
        "due": "2/9/2018"
      }, {
        "id": 26,
        "ticket": "Asoka",
        "title": "incubate end-to-end paradigms",
        "description": "Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\n\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.",
        "phase": {
          "id": 3,
          "title": "Southern California Edison Company"
        },
        "due": "7/14/2018"
      }, {
        "id": 27,
        "ticket": "Alphazap",
        "title": "incentivize best-of-breed models",
        "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.",
        "phase": {
          "id": 2,
          "title": "United Fire Group, Inc"
        },
        "due": "1/18/2019"
      }, {
        "id": 28,
        "ticket": "Bitwolf",
        "title": "engage transparent ROI",
        "description": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        "phase": {
          "id": 2,
          "title": "Logitech International S.A."
        },
        "due": "4/7/2017"
      }, {
        "id": 29,
        "ticket": "Cardguard",
        "title": "reinvent turn-key e-services",
        "description": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.",
        "phase": {
          "id": 1,
          "title": "Host Hotels & Resorts, Inc."
        },
        "due": "9/27/2018"
      }, {
        "id": 30,
        "ticket": "Ventosanzap",
        "title": "visualize collaborative content",
        "description": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.",
        "phase": {
          "id": 3,
          "title": "Nuveen New York Quality Municipal Income Fund"
        },
        "due": "11/12/2019"
      }, {
        "id": 31,
        "ticket": "Vagram",
        "title": "exploit 24/7 e-commerce",
        "description": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
        "phase": {
          "id": 1,
          "title": "Balchem Corporation"
        },
        "due": "8/20/2018"
      }, {
        "id": 32,
        "ticket": "Rank",
        "title": "iterate turn-key users",
        "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.",
        "phase": {
          "id": 2,
          "title": "Universal Stainless & Alloy Products, Inc."
        },
        "due": "11/1/2019"
      }, {
        "id": 33,
        "ticket": "Rank",
        "title": "monetize cutting-edge methodologies",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.",
        "phase": {
          "id": 1,
          "title": "ChemoCentryx, Inc."
        },
        "due": "5/26/2019"
      }, {
        "id": 34,
        "ticket": "Zaam-Dox",
        "title": "exploit granular infrastructures",
        "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.",
        "phase": {
          "id": 1,
          "title": "Sierra Wireless, Inc."
        },
        "due": "8/17/2019"
      }, {
        "id": 35,
        "ticket": "Regrant",
        "title": "harness mission-critical deliverables",
        "description": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
        "phase": {
          "id": 3,
          "title": "RCM Technologies, Inc."
        },
        "due": "12/27/2018"
      }, {
        "id": 36,
        "ticket": "Domainer",
        "title": "utilize e-business channels",
        "description": "Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.",
        "phase": {
          "id": 1,
          "title": "PennantPark Investment Corporation"
        },
        "due": "1/17/2018"
      }, {
        "id": 37,
        "ticket": "Transcof",
        "title": "brand extensible users",
        "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.",
        "phase": {
          "id": 3,
          "title": "Columbia Seligman Premium Technology Growth Fund, Inc"
        },
        "due": "3/28/2017"
      }, {
        "id": 38,
        "ticket": "Subin",
        "title": "mesh 24/7 markets",
        "description": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.",
        "phase": {
          "id": 3,
          "title": "Hamilton Bancorp, Inc."
        },
        "due": "11/24/2019"
      }, {
        "id": 39,
        "ticket": "Zoolab",
        "title": "syndicate impactful initiatives",
        "description": "Sed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
        "phase": {
          "id": 2,
          "title": "B.O.S. Better Online Solutions"
        },
        "due": "7/9/2017"
      }, {
        "id": 40,
        "ticket": "Zamit",
        "title": "reintermediate world-class supply-chains",
        "description": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\n\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.",
        "phase": {
          "id": 1,
          "title": "Hawkins, Inc."
        },
        "due": "8/6/2018"
      }, {
        "id": 41,
        "ticket": "Ronstring",
        "title": "whiteboard efficient supply-chains",
        "description": "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.",
        "phase": {
          "id": 3,
          "title": "Caesars Acquisition Company"
        },
        "due": "10/2/2019"
      }, {
        "id": 42,
        "ticket": "Lotstring",
        "title": "iterate efficient networks",
        "description": "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.",
        "phase": {
          "id": 1,
          "title": "Evercore Partners Inc"
        },
        "due": "7/18/2017"
      }, {
        "id": 43,
        "ticket": "Lotlux",
        "title": "recontextualize user-centric e-tailers",
        "description": "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
        "phase": {
          "id": 1,
          "title": "Wal-Mart Stores, Inc."
        },
        "due": "10/15/2019"
      }, {
        "id": 44,
        "ticket": "Y-Solowarm",
        "title": "incentivize user-centric infomediaries",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.",
        "phase": {
          "id": 1,
          "title": "Exelixis, Inc."
        },
        "due": "3/28/2018"
      }, {
        "id": 45,
        "ticket": "Voyatouch",
        "title": "embrace integrated ROI",
        "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
        "phase": {
          "id": 1,
          "title": "Peapack-Gladstone Financial Corporation"
        },
        "due": "4/28/2019"
      }, {
        "id": 46,
        "ticket": "Ronstring",
        "title": "redefine scalable portals",
        "description": "Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.",
        "phase": {
          "id": 2,
          "title": "Goldman Sachs Group, Inc. (The)"
        },
        "due": "10/29/2019"
      }, {
        "id": 47,
        "ticket": "Regrant",
        "title": "synergize revolutionary e-commerce",
        "description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
        "phase": {
          "id": 1,
          "title": "Independence Holding Company"
        },
        "due": "12/23/2018"
      }, {
        "id": 48,
        "ticket": "Viva",
        "title": "integrate world-class interfaces",
        "description": "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.",
        "phase": {
          "id": 2,
          "title": "SodaStream International Ltd."
        },
        "due": "10/10/2017"
      }, {
        "id": 49,
        "ticket": "Bytecard",
        "title": "monetize best-of-breed relationships",
        "description": "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.",
        "phase": {
          "id": 2,
          "title": "Sanderson Farms, Inc."
        },
        "due": "9/9/2017"
      }, {
        "id": 50,
        "ticket": "Y-find",
        "title": "utilize distributed ROI",
        "description": "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
        "phase": {
          "id": 3,
          "title": "National Grid Transco, PLC"
        },
        "due": "4/16/2017"
      }, {
        "id": 51,
        "ticket": "Sonair",
        "title": "brand plug-and-play infomediaries",
        "description": "Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.",
        "phase": {
          "id": 3,
          "title": "Novelion Therapeutics Inc. "
        },
        "due": "12/16/2017"
      }, {
        "id": 52,
        "ticket": "Zathin",
        "title": "evolve mission-critical supply-chains",
        "description": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.",
        "phase": {
          "id": 3,
          "title": "First Trust Dynamic Europe Equity Income Fund"
        },
        "due": "3/13/2019"
      }, {
        "id": 53,
        "ticket": "Matsoft",
        "title": "redefine cutting-edge niches",
        "description": "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
        "phase": {
          "id": 3,
          "title": "Synchronoss Technologies, Inc."
        },
        "due": "8/28/2019"
      }, {
        "id": 54,
        "ticket": "Subin",
        "title": "target collaborative e-markets",
        "description": "Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
        "phase": {
          "id": 3,
          "title": "County Bancorp, Inc."
        },
        "due": "5/22/2017"
      }, {
        "id": 55,
        "ticket": "Hatity",
        "title": "integrate 24/365 platforms",
        "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "phase": {
          "id": 2,
          "title": "Arlington Asset Investment Corp"
        },
        "due": "11/22/2017"
      }, {
        "id": 56,
        "ticket": "Alphazap",
        "title": "engineer cross-platform web-readiness",
        "description": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.",
        "phase": {
          "id": 2,
          "title": "Prudential Public Limited Company"
        },
        "due": "9/16/2018"
      }, {
        "id": 57,
        "ticket": "Keylex",
        "title": "streamline clicks-and-mortar deliverables",
        "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "phase": {
          "id": 2,
          "title": "Ballard Power Systems, Inc."
        },
        "due": "1/27/2017"
      }, {
        "id": 58,
        "ticket": "Keylex",
        "title": "iterate best-of-breed eyeballs",
        "description": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
        "phase": {
          "id": 2,
          "title": "Gibraltar Industries, Inc."
        },
        "due": "3/4/2018"
      }, {
        "id": 59,
        "ticket": "Sonsing",
        "title": "whiteboard B2C e-tailers",
        "description": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.",
        "phase": {
          "id": 2,
          "title": "Era Group, Inc."
        },
        "due": "2/9/2017"
      }, {
        "id": 60,
        "ticket": "Rank",
        "title": "transition strategic experiences",
        "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "phase": {
          "id": 2,
          "title": "Insys Therapeutics, Inc."
        },
        "due": "7/30/2018"
      }, {
        "id": 61,
        "ticket": "Pannier",
        "title": "engineer ubiquitous systems",
        "description": "Sed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
        "phase": {
          "id": 1,
          "title": "Murphy USA Inc."
        },
        "due": "6/9/2018"
      }, {
        "id": 62,
        "ticket": "Asoka",
        "title": "transform wireless platforms",
        "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
        "phase": {
          "id": 2,
          "title": "Assured Guaranty Ltd."
        },
        "due": "6/15/2017"
      }, {
        "id": 63,
        "ticket": "Subin",
        "title": "e-enable end-to-end communities",
        "description": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
        "phase": {
          "id": 1,
          "title": "Corcept Therapeutics Incorporated"
        },
        "due": "11/29/2017"
      }, {
        "id": 64,
        "ticket": "Redhold",
        "title": "drive distributed e-business",
        "description": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
        "phase": {
          "id": 2,
          "title": "Western Asset Bond Fund"
        },
        "due": "4/29/2017"
      }, {
        "id": 65,
        "ticket": "Tres-Zap",
        "title": "deploy customized experiences",
        "description": "Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.",
        "phase": {
          "id": 1,
          "title": "Janus Henderson Small Cap Growth Alpha ETF"
        },
        "due": "12/22/2017"
      }, {
        "id": 66,
        "ticket": "Tampflex",
        "title": "innovate robust platforms",
        "description": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
        "phase": {
          "id": 3,
          "title": "Entertainment Gaming Asia Incorporated"
        },
        "due": "10/24/2018"
      }, {
        "id": 67,
        "ticket": "Otcom",
        "title": "facilitate B2C e-services",
        "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.",
        "phase": {
          "id": 3,
          "title": "Genco Shipping & Trading Limited Warrants Expiring 12/31/2021 "
        },
        "due": "1/6/2019"
      }, {
        "id": 68,
        "ticket": "Keylex",
        "title": "orchestrate strategic supply-chains",
        "description": "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.",
        "phase": {
          "id": 1,
          "title": "Glaukos Corporation"
        },
        "due": "1/5/2018"
      }, {
        "id": 69,
        "ticket": "Wrapsafe",
        "title": "extend best-of-breed applications",
        "description": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
        "phase": {
          "id": 1,
          "title": "Cadiz, Inc."
        },
        "due": "2/1/2017"
      }, {
        "id": 70,
        "ticket": "It",
        "title": "empower turn-key functionalities",
        "description": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.",
        "phase": {
          "id": 1,
          "title": "United Bancshares, Inc."
        },
        "due": "9/17/2017"
      }, {
        "id": 71,
        "ticket": "Veribet",
        "title": "harness efficient paradigms",
        "description": "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.",
        "phase": {
          "id": 1,
          "title": "Apollo Senior Floating Rate Fund Inc."
        },
        "due": "8/3/2017"
      }, {
        "id": 72,
        "ticket": "Sub-Ex",
        "title": "drive 24/7 content",
        "description": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.",
        "phase": {
          "id": 1,
          "title": "Hawaiian Electric Industries, Inc."
        },
        "due": "10/29/2017"
      }, {
        "id": 73,
        "ticket": "Stronghold",
        "title": "extend seamless e-tailers",
        "description": "Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.",
        "phase": {
          "id": 3,
          "title": "Walter Investment Management Corp."
        },
        "due": "4/11/2018"
      }, {
        "id": 74,
        "ticket": "Konklux",
        "title": "monetize B2C deliverables",
        "description": "Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.",
        "phase": {
          "id": 3,
          "title": "Affiliated Managers Group, Inc."
        },
        "due": "5/13/2017"
      }, {
        "id": 75,
        "ticket": "Vagram",
        "title": "extend leading-edge solutions",
        "description": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.",
        "phase": {
          "id": 2,
          "title": "Western Asset Managed Municipals Fund, Inc."
        },
        "due": "4/2/2018"
      }, {
        "id": 76,
        "ticket": "Subin",
        "title": "unleash sexy architectures",
        "description": "Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.",
        "phase": {
          "id": 3,
          "title": "Arlington Asset Investment Corp"
        },
        "due": "4/13/2017"
      }, {
        "id": 77,
        "ticket": "Job",
        "title": "morph innovative action-items",
        "description": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
        "phase": {
          "id": 1,
          "title": "Barington/Hilco Acquisition Corp."
        },
        "due": "7/24/2019"
      }, {
        "id": 78,
        "ticket": "Cardify",
        "title": "implement frictionless vortals",
        "description": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.",
        "phase": {
          "id": 2,
          "title": "Artisan Partners Asset Management Inc."
        },
        "due": "9/3/2018"
      }, {
        "id": 79,
        "ticket": "Keylex",
        "title": "syndicate enterprise solutions",
        "description": "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.",
        "phase": {
          "id": 2,
          "title": "ConocoPhillips"
        },
        "due": "6/9/2018"
      }, {
        "id": 80,
        "ticket": "Bytecard",
        "title": "reintermediate transparent synergies",
        "description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.",
        "phase": {
          "id": 3,
          "title": "Olympic Steel, Inc."
        },
        "due": "2/21/2019"
      }, {
        "id": 81,
        "ticket": "Domainer",
        "title": "optimize world-class synergies",
        "description": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.",
        "phase": {
          "id": 1,
          "title": "SK Telecom Co., Ltd."
        },
        "due": "3/7/2019"
      }, {
        "id": 82,
        "ticket": "Redhold",
        "title": "visualize collaborative technologies",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.",
        "phase": {
          "id": 2,
          "title": "SunTrust Banks, Inc."
        },
        "due": "10/12/2017"
      }, {
        "id": 83,
        "ticket": "Treeflex",
        "title": "incentivize intuitive supply-chains",
        "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.",
        "phase": {
          "id": 3,
          "title": "Pacific Special Acquisition Corp."
        },
        "due": "11/13/2018"
      }]
}