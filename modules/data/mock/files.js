export function files()  {
    return [
        {
            id: 10001,
            bucket: 'Private',
            name: 'Test project1 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10002,
            bucket: 'Private',
            name: 'Test project2 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10003,
            bucket: 'Public',
            name: 'Test project3 image',
            filetype: 'pdf',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10004,
            bucket: 'Public',
            name: 'Test project4 image',
            filetype: 'xls',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-05-2018'
        },
        {
            id: 10005,
            bucket: 'Private',
            name: 'Test project5 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-30-2018'
        },
        {
            id: 10006,
            bucket: 'Private',
            name: 'Test project6 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-22-2018'
        },
        {
            id: 10007,
            bucket: 'Private',
            name: 'Test project7 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-14-2018'
        },
        {
            id: 10008,
            bucket: 'Private',
            name: 'Test project8 image',
            filetype: 'jpeg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-16-2018'
        },
        {
            id: 10001,
            bucket: 'Private',
            name: 'Test project1 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10002,
            bucket: 'Private',
            name: 'Test project2 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10003,
            bucket: 'Public',
            name: 'Test project3 image',
            filetype: 'pdf',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10004,
            bucket: 'Public',
            name: 'Test project4 image',
            filetype: 'xls',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-05-2018'
        },
        {
            id: 10005,
            bucket: 'Private',
            name: 'Test project5 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-30-2018'
        },
        {
            id: 10006,
            bucket: 'Private',
            name: 'Test project6 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-22-2018'
        },
        {
            id: 10007,
            bucket: 'Private',
            name: 'Test project7 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-14-2018'
        },
        {
            id: 10008,
            bucket: 'Private',
            name: 'Test project8 image',
            filetype: 'jpeg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-16-2018'
        },
        {
            id: 10001,
            bucket: 'Private',
            name: 'Test project1 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10002,
            bucket: 'Private',
            name: 'Test project2 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10003,
            bucket: 'Public',
            name: 'Test project3 image',
            filetype: 'pdf',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10004,
            bucket: 'Public',
            name: 'Test project4 image',
            filetype: 'xls',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-05-2018'
        },
        {
            id: 10005,
            bucket: 'Private',
            name: 'Test project5 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-30-2018'
        },
        {
            id: 10006,
            bucket: 'Private',
            name: 'Test project6 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-22-2018'
        },
        {
            id: 10007,
            bucket: 'Private',
            name: 'Test project7 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-14-2018'
        },
        {
            id: 10008,
            bucket: 'Private',
            name: 'Test project8 image',
            filetype: 'jpeg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-16-2018'
        },
        {
            id: 10001,
            bucket: 'Private',
            name: 'Test project1 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10002,
            bucket: 'Private',
            name: 'Test project2 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10003,
            bucket: 'Public',
            name: 'Test project3 image',
            filetype: 'pdf',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-12-2018'
        },
        {
            id: 10004,
            bucket: 'Public',
            name: 'Test project4 image',
            filetype: 'xls',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-05-2018'
        },
        {
            id: 10005,
            bucket: 'Private',
            name: 'Test project5 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-30-2018'
        },
        {
            id: 10006,
            bucket: 'Private',
            name: 'Test project6 image',
            filetype: 'jpg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-22-2018'
        },
        {
            id: 10007,
            bucket: 'Private',
            name: 'Test project7 image',
            filetype: 'png',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-14-2018'
        },
        {
            id: 10008,
            bucket: 'Private',
            name: 'Test project8 image',
            filetype: 'jpeg',
            path: 'https://cdn-images-1.medium.com/max/1200/1*-8AAdexfOAK9R-AIha_PBQ.png',
            createddate: '10-16-2018'
        },
    ]
}